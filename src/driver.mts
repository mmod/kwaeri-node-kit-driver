/**
 * SPDX-PackageName: kwaeri/driver
 * SPDX-PackageVersion: 0.6.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'


// INCLUDES
import { DriverConnectionBits, DatabaseDriver } from '@kwaeri/database-driver';
import debug from 'debug';


// DEFINES
const DEBUG = debug( 'kwaeri:driver' );


export class Driver {
    /**
     * @var { connectionBits } connection
     */
    protected  connection: DriverConnectionBits;


    /**
     * @var { DatabaseDriver } dbo
     */
    protected  dbo: DatabaseDriver;


    /**
     * Class constructor
     */
    constructor( config: DriverConnectionBits, driver: new( conf: DriverConnectionBits ) => DatabaseDriver ) {
        DEBUG( `DriverConnectionBits: '${config}'` );

        this.connection = config;

        DEBUG( `Call new on '${driver}'` );

        this.dbo = new driver( config );

        if( this.dbo === undefined || this.dbo === null )
            throw new Error( `A valid constructor for a 'DatabaseDriver' or one of its descendants must be provided.` );
    }


    /**
     * Method to get the dbo
     *
     * @param { void }
     *
     * @return { DatabaseDriver } A handle to the dbo
     */
    get get(): DatabaseDriver {
        DEBUG( `Get 'instance' of '${this.connection.type}' database driver` );

        return this.dbo;
    }


    /**
     * Method to set the dbo
     *
     * @param { new( conf: DriverConnectionBits ) => DatabaseDriver } driver A method which returns a DatabaseDriver or derivative.
     */
    set set( driver: new( conf: DriverConnectionBits ) => DatabaseDriver ) {
        DEBUG( `Set 'instance' of '${this.connection.type}' database driver` );

        this.dbo = new driver( this.connection );

        if( this.dbo === undefined || this.dbo === null )
            throw new Error( `A valid constructor for a 'DatabaseDriver' or one of its descendants must be provided.` );
    }
}

