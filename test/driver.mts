/**
 * SPDX-PackageName: kwaeri/driver
 * SPDX-PackageVersion: 0.6.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'


// INCLUDES
import * as assert from 'assert';
import { Driver } from '../src/driver.mjs';
import { DriverConnectionBits, DatabaseDriver, QueryResult } from '@kwaeri/database-driver';
import debug from 'debug';


// DEFINES
const DEBUG = debug( 'nodekit:driver-test' );

const Derived = class DerivedDriver extends DatabaseDriver{
    constructor( config: DriverConnectionBits ) {
        super( config );
    }

    query( query: string ): Promise<any> {
        return Promise.resolve( { rows: [], fields: [] } )
    }
},
connectionBits: DriverConnectionBits = { host: "fake", port: 5000, database: "nonexistent", user: "not_real", password: "empty", type: "derived" },
driver = new Driver( connectionBits, Derived ),
dbo = driver.get;


// SANITY CHECK - Makes sure our tests are working proerly
describe(
    'PREREQUISITE',
    () => {
        describe(
            'Sanity Test(s)',
            () => {

                it(
                    'Should return true.',
                    () => {
                        assert.equal( [1,2,3,4].indexOf(4), 3 );
                    }
                );

            }
        );
    }
);


// Create table test
describe(
    'Driver Functionality Test Suite',
    () => {

        describe(
            'Concept Test',
            () => {
                it(
                    'Should return a promise with empty rows and fields arrays.',
                    async () => {
                        try {
                            const result = await dbo.query( "" );

                            return Promise.resolve(
                                assert.equal(
                                    JSON.stringify( result ),
                                    JSON.stringify( { rows: [], fields: [] } )
                                )
                            );
                        }
                        catch( error ) {
                            DEBUG( `Error: ${error}`)

                            return Promise.reject( `${error}` );
                        }
                    }
                );
            }
        );

    }
);
