# [![Patreon](https://img.shields.io/badge/Patreon-Funding-inactive?style=for-the-badge&logo=patreon&color=FF424D)](https://patreon.com/kirvedx) kwaeri-node-kit-driver [![PayPal](https://img.shields.io/badge/PayPal-Donations-inactive?style=for-the-badge&logo=paypal&color=253B80)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)

A Massively Modified Open Source Project by kirvedx

[![GPG/Keybase](https://img.shields.io/badge/GPG-1B842CB5%20Rik-inactive?style=for-the-badge&label=GnuPG2%2FKeybase&logo=gnu+privacy+guard&color=0093dd)](https://keybase.io/rik)
[![Google](https://img.shields.io/badge/Google%20Developers-kirvedx-inactive?style=for-the-badge&logo=google+tag+manager&color=414141)](https://developers.google.com/profile/u/117028112450485835638)
[![GitLab](https://img.shields.io/badge/GitLab-kirvedx-inactive?style=for-the-badge&logo=gitlab&color=fca121)](https://github.com/kirvedx)
[![GitHub](https://img.shields.io/badge/GitHub-kirvedx-inactive?style=for-the-badge&logo=github&color=181717)](https://github.com/kirvedx)
[![npm](https://img.shields.io/badge/NPM-Rik-inactive?style=for-the-badge&logo=npm&color=CB3837)](https://npmjs.com/~rik)

The @kwaeri/driver component for the @kwaeri/node-kit application platform

[![pipeline status](https://gitlab.com/kwaeri/node-kit/driver/badges/main/pipeline.svg)](https://gitlab.com/kwaeri/node-kit/driver/commits/main)  [![coverage report](https://gitlab.com/kwaeri/node-kit/driver/badges/main/coverage.svg)](https://kwaeri.gitlab.io/node-kit/driver/coverage/)  [![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/1879/badge)](https://bestpractices.coreinfrastructure.org/projects/1879)

## TOC
* [The Implementation](#the-implementation)
* [Getting Started](#getting-started)
  * [Installation](#installation)
  * [Include the Component](#include-the-component)
  * [Provide a Configuration](#provide-a-configuration)
* [Usage](#usage)
  * [Query the Database](#query-the-database)
* [How to Contribute Code](#how-to-contribute-code)
* [Other Ways to Contribute](#other-ways-to-contribute)
  * [Bug Reports](#bug-reports)
  * [Vulnerability Reports](#vulnerability-reports)
    * [Confidential Issues](#confidential-issues)
  * [Donations](#donations)

## The Implementation

@kwaeri/driver reinvents and modernizes the driver portion of the nk application platform.

As the driver component was originally baked into the nk module, its usage was entirely controlled by it. As we discern the process for decoupling the individual components which make up a kwaeri application, we'll begin to simplify the act of doing so, and provide documentation for utilizing each component individually.

The documentation is continually deployed, and located at the [Gitlab Hosted Documentation for @kwaeri/driver](https://kwaeri.gitlab.io/node-kit/driver/)

## Getting Started

[@kwaeri/node-kit](https://www.npmjs.com/package/@kwaeri/node-kit) wraps the various components under the kwaeri scope necessary for building a kwaeri application, and provides a single entry point for easing the process of building a kwaeri application.

[@kwaeri/cli](https://www.npmjs.com/package/@kwaeri/cli) wraps the various CLI components under the @kwaeri scope, and provides a single entry point to the user executable framework.

However, if you wish to install @kwaeri/driver and utilize it specifically - perform the following steps to get started:

### Installation

Install @kwaeri/driver:

```bash
npm install @kwaeri/driver
```

### Include the Component

To leverage the database driver, you'll first need to include it:

```javascript
// INCLUDES
import { Driver } from '@kwaeri/driver';
```

### Provide a configuration

In order to instantiate a driver object, you'll need to provide a configuration for a database type, as well as provide the appropriate database driver provider to the driver module constructor. Here's an example configuration for both MySQL and PostgreSQL:

**MySQL**

```typescript
const conf = {
  type:     "mysql",
  host:     "localhost",
  port:     3306,
  database: "dbname",
  user:     "root",
  pass:     "password"
};
```

**Postgres**

```typescript
const conf = {
  type:     "postgresql",
  host:     "localhost", // or postgres
  port:     5432,
  database: "dbname",
  user:     "dbuser",
  password: "password"
};
```


#### Using the Driver

To use the Driver, you must supply the Driver constructor with the configuration created in the previous step - AND a database driver provider which is derived from @kwaeri/database-driver's `DatabaseDriver` that implements the `BaseDriver` interface. Here's an example in full:

```javascript
import { MySQLDriver } from '@kwaeri/mysql-database-driver';
import { Driver } from '@kwaeri/driver';

const driver = new Driver( conf, MySQLDriver );
```

Notice that the driver provider *type* is what is passed. Now we can utilize the driver provider anywhere you have access to the Driver module:

```javascript
const dbo = driver.get;

const { rows, fields } = await dbo.query( "..." );

[...]
```

#### Providing a Custom Driver

The Driver component module exists, and in such a way, so as to decouple dependencies from a derived @kwaeri/node-kit application. In our constructor, the provider "`driver`" type is typed as a constructor that returns a type of `DatabaseDriver`, which can be satisfied by *any* database-provider that inherits from said type;  @kwaeri/session does something extremely similar with it's session-store providers. Polymorphism ftw!

This means it's super easy to utilize any database provider with the Driver module. This is essentially done in hopes of standardizing database integration such that all aspects of a kwaeri/node-kit application will work seamlessly regardless of the database provider used.

**NOTE**
It isn't necessarily inapproparite - at this time - to foregoe the Driver module altogether, and leverage a database provider directly - especially if your product simply won't integrate with out major changes that break seamless functionality across different database providers. In the future, we'll be providing an elogquent API for each database provider - such that all databse providers can seamlessly interchange (including nosql providers).

Where we even make use of the Driver module, is in a derived nodekit application we've termed the `xrm`, or (x)Cross-Relational Management system. Think of it like a CMS for anyone that caters to developers as end users.

To provide a custom provider to the Driver module, you'll need to derive one from the DatabaseDriver class (found in @kwaeri/database-driver), overriding the `query` method appropriately if possible.  An example driver implementation can be found by viewing the [source of the MySQLDriver](https://www.gitlab.com/kwaeri/cli/providers/mysql-database-driver)

Once you've created your implementation, or found one you'd like to leverage with kwaeri/driver; You must pass a configuration - and the respective database-driver - to the Driver constructor so it may be consumed:

```javascript
// INCLUDES:
import { CustomDriver } from "../my/custom/driver";

// DEFINES:
const conf = {
  type:     'custom',
  host:     'localhost',
  database: 'dbname',
  user:     'root',
  password: 'password'
};

const driver = new Driver( conf, CustomDriver );

// Access the stored dbo with the built-in getter
const dbo = driver.get;
```

The Driver class will store, wrap, and make use of your custom databse driver implementation.

## Usage

We have not discerned the entire process one will need to follow in order to make use of @kwaeri/driver individually. The component was originally intended to be used in conjunction with several other components which make up the nk application platform.

More documentation to come!

## How to Contribute Code

Our Open Source projects are always open to contribution. If you'd like to cocntribute, all we ask is that you follow the guidelines for contributions, which can be found at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Contribute-Code)

There you'll find topics such as the guidelines for contributions; step-by-step walk-throughs for getting set up, [Coding Standards](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Coding-Standards), [CSS Naming Conventions](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/CSS-Naming-Conventions), and more.

The project also leverages Keybase for communication and alerts - outside of standard email. To join our keybase chat, run the following from terminal (assuming you have [keybase](https://www.keybase.io) installed and running):

```bash
keybase team request-access kwaeri
```

Alternatively, you could search for the team in the GUI application and request access from there.

## Other Ways to Contribute

There are other ways to contribute to the project other than with code. Consider [testing](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Test-Code) the software, or in case you've found an [Bug](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports) - please report it. You can also support the project monetarly through [donations](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Donations) via PayPal.

Regardless of how you'd like to contribute, you can also find in-depth information for how to do so at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute)

### Bug Reports

To submit bug reports, request enhancements, and/or new features - please make use of the **issues** system baked-in to our source control project space at [Gitlab](https://gitlab.com/mmod/kwaeri-node-kit/issues)

You may optionally start an issue, track, and manage it via email by sending an email to our project's [support desk](mailto:incoming+mmod/kwaeri-node-kit@incoming.gitlab.com).

For more in-depth documentation on the process of submitting bug reports, please visit the [Massively Modified Wiki on Bug Reports](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports)

### Vulnerability Reports

Our Vulnerability Reporting process is very similar to Gitlab's. In fact, you could say its a *fork*.

To submit vulnerability reports, please email our [Security Group](mailto:security@mmod.co). We will try to acknowledge receipt of said vulnerability by the next business day, and to also provide regular updates about our progress. If you are curious about the status of your report feel free to email us again. If you wish to encrypt your disclosure email, like with gitlab - please email us to ask for our GPG Key.

Please refrain from requesting compensation for reporting vulnerabilities. We will publicly acknowledge your responsible disclosure, if you request us to do so. We will also try to make the confidential issue public after the vulnerability is announced.

You are not allowed, and will not be able, to search for vulnerabilities on Gitlab.com. As our software is open source, you may download a copy of the source and test against that.

#### Confidential Issues

When a vulnerability is discovered, we create a [confidential issue] to track it internally. Security patches will be pushed to private branches and eventually merged into a `security` branch. Security issues that are not vulnerabilites can be seen on our [public issue tracker](https://gitlab.com/mmod/kwaeri-node-kit/issues?label_name%5B%5D=Security).

For more in-depth information regarding vulnerability reports, confidentiality, and our practices; Please visit the [Massively Modified Wiki on Vulnerability](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Vulnerability-Reports)

### Donations

If you cannot contribute time or energy to neither the code base, documentation, nor community support; please consider making a monetary contribution which is extremely useful for maintaining the Massively Modified network and all the goodies offered free to the public.

[![Donate via PayPal.com](https://gitlab.com/mmod/kwaeri-user-experience/raw/master/images/mmod-donate-btn-2.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)
